<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Event;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {

    }

    protected function render($view, $data = [])
    {

        return view(
            $view,
            [
                'data' => $data,
            ]
        );
    }

    public function index()
    {
        $events_list = $this->getEvents([
            ['link', '!=', ''],
            ['link', '!=', NULL],
			['header', '!=', ''],
            ['header', '!=', NULL],
            ['date', '!=', ''],
            ['date', '!=', NULL],
        ]);

        $pubs_list = $this->getPubs([
            ['header', '!=', ''],
            ['header', '!=', NULL],
            ['title', '!=', ''],
            ['title', '!=', NULL],
        ]);

        return $this->render(
            'index',
            [
                'events_list' => $events_list,
                'pubs_list' => $pubs_list,
            ]
        );
    }

    private function getEvents($Where)
    {
        return Event::where($Where)
        ->orderBy('date', 'desc')
        ->get();
    }

    private function getPubs($Where)
    {
        return Event::where($Where)
        ->orderBy('date', 'desc')
        ->get();
    }
}
