<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Los Santos Informer</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="icon" type="image/png" href="https://cdn.discordapp.com/attachments/412569678309883904/413267828804812802/allnews.jpg" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>

        <div class="home-page container">
            <div class="home-page-title row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <h1>Los Santos Informer</h1>
                </div>
            </div>

            <div class="home-page-pubs row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                  <h1>Publicités</h1>
                    <div class="container">
                      <div id="slider-pubs" class="slider-pubs slide-pubs" data-ride="slider-pubs">

                        <!-- Wrapper for slides -->
                        <div class="slider-pubs-inner">
                          <div class="item-pubs active">
                            <img src="https://cdn.discordapp.com/attachments/298784091233452033/411179574060384268/chinotto.png">
                            <div class="slider-pub-caption">
                              <h3>Votre pubs ici !</h3>
                            </div>
                          </div>

                        @foreach($data['events_list'] as $event)
                          <div class="item-pubs">
                            <img src="@php echo $event['attributes']['header']; @endphp">
                            <div class="slider-pubs-caption">
                              <h3>@php echo $event['attributes']['title']; @endphp</h3>
                              <p>Le @php echo date("d-m-Y", strtotime($event['attributes']['date'])); @endphp</p>
                            </div>
                          </div>
                        @endforeach
                        </div>

                        <!-- Left and right controls -->
                        <a class="left slider-pubs-control" href="#slider-pubs" data-slide-pubs="prev">
                          <span class="glyphicon glyphicon-chevron-left"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="right slider-pubs-control" href="#slider-pubs" data-slide-pubs="next">
                          <span class="glyphicon glyphicon-chevron-right"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                    </div>
                </div>
            </div>

            <div class="home-page-news row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <h1>News</h1>
                </div>
            </div>

            <div class="home-page-slider-events row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                  <h1>Evènements en ville</h1>
                    <div class="container">
                      <div id="slider-events" class="slider-events slide-events" data-ride="slider-event">

                        <!-- Wrapper for slides -->
                        <div class="slider-event-inner">
                          <div class="item-events active">
                            <img src="https://cdn.discordapp.com/attachments/298784091233452033/411179574060384268/chinotto.png">
                            <div class="slider-event-caption">
                              <h3>Un évènements spécial à organiser ?</h3>
                            </div>
                          </div>

                        @foreach($data['events_list'] as $event)
                          <div class="item-events">
                            <img src="@php echo $event['attributes']['header']; @endphp">
                            <div class="slider-event-caption">
                              <h3>@php echo $event['attributes']['title']; @endphp</h3>
                            </div>
                          </div>
                        @endforeach
                        </div>

                        <!-- Left and right controls -->
                        <a class="left slider-event-control" href="#slider-event" data-slide-events="prev">
                          <span class="glyphicon glyphicon-chevron-left"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="right slider-event-control" href="#slider-event" data-slide-events="next">
                          <span class="glyphicon glyphicon-chevron-right"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                    </div>
                </div>
            </div>

            <div class="home-page-flux row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">

                </div>
            </div>
        </div>

    </body>
</html>
